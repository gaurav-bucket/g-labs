﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using G_Labs.Models;

namespace G_Labs.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Journey()
        {
            SetUniqueUserID();
            return Redirect("/what-exactly-is-devops-and-why-almost-every-software-company-is-adopting-it");
            //return View();
        }
        private void SetUniqueUserID()
        {
            if (Request.Cookies["userID"] != null)
            {
                return;
            }
            string userID = string.Empty;
            Guid g;
            g = Guid.NewGuid();
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("userID", g.ToString(), option);

        }
        public IActionResult Index()  
        {
            return View();
        }
        [Route("login")]
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Chapters() 
        {
            return View();
        }
        

        [Route("styles")]
        public IActionResult Styles()
        {
            return View();
        }

        #region Project Zoro Chapters
        [Route("/what-exactly-is-devops-and-why-almost-every-software-company-is-adopting-it")]
        public IActionResult Zoro_01()
        {
            return View();
        }
        [Route("/create-a-simple-aspnetcore-application")]
        public IActionResult Zoro_02()
        {
            return View();
        }

        [Route("/how-to-deploy-aspnetcore-application-on-microsoft-azure")]
        public IActionResult Zoro_03()
        {
            return View();
        }

        [Route("/how-to-use-git-as-your-code-repository")]
        public IActionResult Zoro_04()
        {
            return View();
        }
        #endregion


    }
}
